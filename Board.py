import re
from copy import deepcopy


class Board:
    verticals = {'v'}

    def __init__(self, position: str):
        self.name = position.strip()
        self.rows = 5
        self.cols = 4
        self.grid = [['0'] * self.cols] * self.rows
        self.grid = Board.read_string(position)
        try:
            self.valid()
        except ValueError as e:
            raise e
        self.moves = None
        self.winning = bool(re.match(r"^.ss. .ss. [01vhs]{4} [01vhs]{4} [01vhs]{4}$", self.name))
        self.starting = self.name == '1001 v11v vhhv vssv vssv'

    def get_all_possible_moves(self):
        if self.moves is not None:
            return self.moves
        else:
            moves = []
            moved_blocks = set()
            for i, row in enumerate(self.grid):
                for k, cell in enumerate(row):
                    if (i, k) in moved_blocks:
                        continue
                    for b in self.get_all_moves_for_block(i, k):
                        moves.append(b)
                    if cell in {'v', 'h', 's'}:
                        moved_blocks.add((i, k))
                        if cell == 'v':
                            moved_blocks.add((i + 1, k))
                        elif cell == 'h':
                            moved_blocks.add((i, k + 1))
                        elif cell == 's':
                            moved_blocks.add((i, k + 1))
                            moved_blocks.add((i + 1, k))
                            moved_blocks.add((i + 1, k + 1))
            self.moves = moves
            return moves

    def get_all_moves_for_block(self, r, c) -> list:
        """
        check if the block at a given cell can move.
        :return: a list of possible new board configurations reached by moving that block.
        """
        if self.grid[r][c] == '0':
            return []
        possibles = []
        if self.grid[r][c] == '1':
            if self.cell_is_empty(r - 1, c):
                possibles.append(self.move_block_up(r, c))
            if self.cell_is_empty(r, c - 1):
                possibles.append(self.move_block_left(r, c))
            if self.cell_is_empty(r, c + 1):
                possibles.append(self.move_block_right(r, c))
            if self.cell_is_empty(r + 1, c):
                possibles.append(self.move_block_down(r, c))

        elif self.grid[r][c] in Board.verticals:
            if self.cell_is_empty(r - 1, c):
                possibles.append(self.move_block_up(r, c))
            if self.cell_is_empty(r, c - 1) and self.cell_is_empty(r + 1, c - 1):
                possibles.append(self.move_block_left(r, c))
            if self.cell_is_empty(r, c + 1) and self.cell_is_empty(r + 1, c + 1):
                possibles.append(self.move_block_right(r, c))
            if self.cell_is_empty(r + 2, c):
                possibles.append(self.move_block_down(r, c))

        elif self.grid[r][c] == 'h':
            if self.cell_is_empty(r - 1, c) and self.cell_is_empty(r - 1, c + 1):
                possibles.append(self.move_block_up(r, c))
            if self.cell_is_empty(r, c - 1):
                possibles.append(self.move_block_left(r, c))
            if self.cell_is_empty(r, c + 2):
                possibles.append(self.move_block_right(r, c))
            if self.cell_is_empty(r + 1, c) and self.cell_is_empty(r + 1, c + 1):
                possibles.append(self.move_block_down(r, c))

        elif self.grid[r][c] == 's':
            if self.cell_is_empty(r - 1, c) and self.cell_is_empty(r - 1, c + 1):
                possibles.append(self.move_block_up(r, c))
            if self.cell_is_empty(r, c - 1) and self.cell_is_empty(r + 1, c - 1):
                possibles.append(self.move_block_left(r, c))
            if self.cell_is_empty(r, c + 2) and self.cell_is_empty(r + 1, c + 2):
                possibles.append(self.move_block_right(r, c))
            if self.cell_is_empty(r + 2, c) and self.cell_is_empty(r + 2, c + 1):
                possibles.append(self.move_block_down(r, c))

        return possibles

    def move_block_up(self, r0, c0):
        return self.move_block(r0, c0, r0 - 1, c0)

    def move_block_down(self, r0, c0):
        return self.move_block(r0, c0, r0 + 1, c0)

    def move_block_left(self, r0, c0):
        return self.move_block(r0, c0, r0, c0 - 1)

    def move_block_right(self, r0, c0):
        return self.move_block(r0, c0, r0, c0 + 1)

    def move_block(self, r0, c0, r1, c1):
        g = deepcopy(self.grid)
        block = g[r0][c0]

        if block == '0':
            raise ValueError("Can't move emptiness")

        if block == '1':
            g[r0][c0] = '0'
            g[r1][c1] = '1'

        elif block == 'v':
            g[r0][c0] = '0'
            g[r0 + 1][c0] = '0'
            g[r1][c1] = block
            g[r1 + 1][c1] = block

        elif block == 'h':
            g[r0][c0] = '0'
            g[r0][c0 + 1] = '0'
            g[r1][c1] = 'h'
            g[r1][c1 + 1] = 'h'

        elif block == 's':
            g[r0][c0] = '0'
            g[r0][c0 + 1] = '0'
            g[r0 + 1][c0] = '0'
            g[r0 + 1][c0 + 1] = '0'

            g[r1][c1] = 's'
            g[r1][c1 + 1] = 's'
            g[r1 + 1][c1] = 's'
            g[r1 + 1][c1 + 1] = 's'

        return Board.from_grid(g)

    def cell_is_empty(self, r, c):
        if not ((0 <= r < self.rows) and (0 <= c < self.cols)):
            return False
        return self.grid[r][c] == '0'

    def get_mirror(self):
        # Sides entirely switched
        mirror = deepcopy(self.grid)
        for row in mirror:
            row.reverse()

        return Board.from_grid(mirror)

    @staticmethod
    def read_string(s: str):
        # String should look like: 1001 a11b ahhb cssd cssd
        pattern = re.compile("^([01vhs]{4} ){4}[01vhs]{4}$")
        if not pattern.match(s.strip()):
            raise ValueError("board state string does not match regex: " + s)

        rows = s.split(" ")
        new_grid = [[c for c in r] for r in rows]
        return new_grid

    @staticmethod
    def from_grid(g):
        s = ''
        for r in g:
            s += ''.join(r) + " "
        return Board(s)

    def prettystr(self):
        s = ""
        for r in self.grid:
            s += " ".join(r) + "\n"
        return s

    def valid(self):
        valid_blocks = set()
        vertical_cells = set()
        empty_count = 0
        one_by_one_count = 0

        for i, row in enumerate(self.grid):
            for k, cell in enumerate(row):

                if cell == '0':
                    empty_count += 1

                if cell == '1':
                    one_by_one_count += 1

                if cell in valid_blocks:
                    continue

                if cell == 'v':
                    if (i, k) in vertical_cells:
                        continue
                    if self.check_contents_same(i + 1, k, cell):
                        vertical_cells.add((i, k))
                        vertical_cells.add((i + 1, k))
                    else:
                        raise ValueError('validation of vertical failed: ' + str((i, k)) + ' in ' + self.name)

                if cell == 'h':
                    if self.check_contents_same(i, k + 1, cell):
                        valid_blocks.add(cell)
                    else:
                        raise ValueError('validation of h failed in ' + self.name)

                if cell == 's':
                    if self.check_contents_same(i + 1, k, cell) and \
                            self.check_contents_same(i, k + 1, cell) and \
                            self.check_contents_same(i + 1, k + 1, cell):
                        valid_blocks.add(cell)
                    else:
                        raise ValueError('validation of s failed in ' + self.name)

        if empty_count == 2:
            valid_blocks.add('0')
        if one_by_one_count == 4:
            valid_blocks.add('1')
        if len(vertical_cells) == 8:
            valid_blocks.add('v')

        if valid_blocks != {'0', '1', 'v', 'h', 's'}:
            raise ValueError("Seems like a block is missing: " + str(valid_blocks))

    def check_contents_same(self, r: int, c: int, original: str) -> bool:
        if not ((0 <= r < self.rows) and (0 <= c < self.cols)):
            return False
        else:
            return self.grid[r][c] == original

    def __eq__(self, other):
        if type(other) is not Board:
            return False
        return self.name == other.name

    def __repr__(self):
        return self.name

    def __hash__(self):
        return self.name.__hash__()

    def __cmp__(self, other):
        return self.name.__cmp__(other.name)

    def __lt__(self, other):
        return self.name.__lt__(other.name)

    def __gt__(self, other):
        return self.name.__gt__(other.name)

