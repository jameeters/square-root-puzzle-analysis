from Positions import Positions
from graphviz import Graph
import json


def run():
    p = Positions()
    p.analyze_all_the_things()
    write_to_file(p)


def write_to_file(p):
    graph_filename = 'graph.json'
    with open(graph_filename, 'w+') as f:
        json.dump({str(k): [str(i) for i in v] for k, v in p.graph.items()}, f, indent=4)

    if p.mirrored_graph is None:
        p.create_mirrored_graph()

    mirrored_graph_filename = 'mirrored_graph.json'
    with open(mirrored_graph_filename, 'w+') as f:
        json.dump({str(k): [str(i) for i in v] for k, v in p.mirrored_graph.items()}, f, indent=4)


def visualize(p):
    g = Graph('square-root')
    listed_edges = set()
    for k, v in p.graph.items():
        for i in v:
            if (i.name, k.name) in listed_edges:
                continue
            else:
                g.edge(k.name, i.name)
                listed_edges.add((k.name, i.name))
    g.save()
    return g


def visualize_mirrored(p):
    graphname = 'mirrored-square-root'
    g = Graph(graphname)
    listed_edges = set()
    for mirror in p.mirrored_graph.keys():
        # g.node(mirror.name, mirror.prettystr(), shape='plaintext')
        g.node(mirror.name, None, shape='point')

    for k, v in p.mirrored_graph.items():
        for i in v:
            if (i.name, k.name) in listed_edges:
                continue
            g.edge(k.name, i.name)
            listed_edges.add((k.name, i.name))
    g.save()
    return g

def visualize_wincon(p):
    graphname = 'wincon-square-root'
    g = Graph(graphname)
    listed_edges = set()
    if p.wincon_graph is None:
        p.condense_winning_states()
    for mirror in p.wincon_graph.keys():
        # g.node(mirror.name, mirror.prettystr(), shape='plaintext')
        g.node(mirror.name, None, shape='point')

    for k, v in p.wincon_graph.items():
        for i in v:
            if (i.name, k.name) in listed_edges:
                continue
            g.edge(k.name, i.name)
            listed_edges.add((k.name, i.name))
    g.save()
    return g

if __name__ == '__main__':
    run()
