from Board import Board
import sys

sys.setrecursionlimit(10**5)


class Mirror:
    index = dict()

    @staticmethod
    def reflect(board):
        try:
            return Mirror.index[board]
        except KeyError:
            try:
                mirror = Mirror.index[board.get_mirror()]
                Mirror.index[board] = mirror
                return mirror
            except KeyError:
                mirror = Mirror(board)
                for board in mirror.boards:
                    Mirror.index[board] = mirror
                return mirror

    def __init__(self, original: Board):
        self.boards = tuple({original, original.get_mirror()})
        self.idempotent = len(self.boards) == 1
        self.name = str(self.boards)
        self.moves = set()
        if len(self.boards) > 1 and (self.boards[0].winning != self.boards[1].winning):
            raise ValueError(f'Winning status of reflections should match: {self.boards}')
        self.winning = self.boards[0].winning
        self.starting = original.starting

    def find_moves(self):
        for board in self.boards:
            for move in board.get_all_possible_moves():
                self.moves.add(Mirror.reflect(move))

    def __eq__(self, other):
        if type(other) is not Mirror:
            return False
        return self.boards == other.boards

    def __str__(self):
        return str(self.boards)

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return self.boards.__hash__()

    def prettystr(self):
        if self.idempotent:
            return self.boards[0].prettystr()
        else:
            split_names = [b.name.split(' ') for b in self.boards]
            row_pairs = list(zip(split_names[0], split_names[1]))
            return '\n'.join(['|'.join(rp) for rp in row_pairs])

class CondensedState:
    def __init__(self, name):
        self.states = set()
        self.name = name
