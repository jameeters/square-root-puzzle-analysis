import json

from Board import Board
from Reflection import Mirror, CondensedState


class Positions:
    # I want positions stored in an undirected graph

    def __init__(self):
        starting_board = Board('1001 v11v vhhv vssv vssv')
        self.graph = {starting_board: set()}
        self.analyzed = set()
        self.mirrored_graph = None
        self.wincon_graph = None

    def get_next(self):
        for position in self.graph.keys():
            if position not in self.analyzed:
                return position

    def analyze_next(self):
        parent = self.get_next()
        children = parent.get_all_possible_moves()
        for c in children:
            self.graph[parent].add(c)
            try:
                self.graph[c].add(parent)
            except KeyError:
                self.graph[c] = {parent}
        self.analyzed.add(parent)

    def analyze_all_the_things(self):
        while self.analyzed != self.graph.keys():
            self.analyze_next()
            if len(self.analyzed) % 10 == 0:
                print(str(len(self.analyzed)) + ' / ' + str(len(self.graph)) + ' = ' + str(
                    len(self.analyzed) / len(self.graph)))
        print("done")

    def create_mirrored_graph(self):
        # Group all the board states together into pairs of mirrors
        mirrored_boards = set()
        for board in self.graph.keys():
            mirrored_boards.add(Mirror.reflect(board))

        for mb in mirrored_boards:
            mb.find_moves()

        self.mirrored_graph = {mb: mb.moves for mb in mirrored_boards}

    def condense_winning_states(self):
        winstate = CondensedState('(winning)')
        if self.mirrored_graph is None:
            self.create_mirrored_graph()

        self.wincon_graph = {winstate: set()}

        for k, v in self.mirrored_graph.items():
            if k.winning:
                self.wincon_graph[winstate].update(v)
                for other in v:
                    try:
                        self.wincon_graph[other].remove(k)
                        self.wincon_graph[other].add(winstate)
                    except KeyError:
                        self.wincon_graph[other] = self.mirrored_graph[other].copy()
                        self.wincon_graph[other].remove(k)
                        self.wincon_graph[other].add(winstate)
            else:
                self.wincon_graph[k] = v.copy()


    @staticmethod
    def from_file(filename):
        with open(filename, 'r') as f:
            p = Positions()
            p.graph = {Board(k): set([Board(i) for i in v]) for k, v in json.load(f).items()}
        return p
